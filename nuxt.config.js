export default {
  telemetry: false,

  head: {
    title: 'Espace sur demande',
    htmlAttrs: {
      lang: 'fr',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/images/favicon.ico' }],
  },

  css: ['~/assets/style/main.css'],

  modules: [
    // ['nuxt-matomo', { matomoUrl: MATOMO_URL, siteId: MATOMO_SITE_ID }],
  ],

  publicRuntimeConfig: {
    mobileBreakpoint: 992,
  },
};
