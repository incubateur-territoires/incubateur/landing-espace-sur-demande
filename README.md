# Espace sur demande

Landing page temporaire pour le site espace sur demande

## Technologies mises en œuvre
Le frontend est en Vue.js 2 avec [Nuxt](https://nuxtjs.org/fr/) comme framework.

## Développement local
Voici les étapes à suivre pour lancer une instance locale du site de l'Incubateur des Territoires.

### Lancer le service
```
git clone https://gitlab.com/incubateur-territoires/incubateur/website-incubateur.git
cd website-incubateur
npm install
npm run dev
```

Le serveur de développement devrait être maintenant accessible sur `localhost:3000`.
