export function trackOutLink(url) {
  if (this.$matomo) {
    this.$matomo.trackLink(url, 'link');
  }
}

export function trackDownloadClik(filename) {
  if (this.$matomo) {
    this.$matomo.trackLink(filename, 'download');
  }
}

export function trackSearch(value) {
  if (this.$matomo) {
    this.$matomo.trackSearch(value);
  }
}

export function trackGoal(id, value) {
  if (this.$matomo) {
    this.$matomo.trackGoal(id, value);
  }
}

export function trackSiteSearch(value) {
  if (this.$matomo) {
    this.$matomo.trackSiteSearch(value);
  }
}

export function trackEvent(category, action, name) {
  if (this.$matomo) {
    this.$matomo.trackEvent(category, action, name);
  }
}

export function setDocumentTitle(url) {
  if (this.$matomo) {
    this.$matomo.setDocumentTitle(url);
  }
}
